Rails.application.routes.draw do

  get 'feedback/index'

  get 'home/index'

  resources :home do
    collection do
      get :login
      get :signup
      get :terms_of_use
      get :recover_pass
    end
  end

  resources :games do
    collection do
      get :games_on
    end
  end

  resources :feedback do
    collection do
      get :no_leagues
      get :no_leagues2
    end
  end

  resources :leagues do
    collection do
      get :details
      get :new2
      get :choose_shield
      get :choose_deco
      get :invite_friends
    end
  end

  resources :myleagues do
    collection do
      get :details
      get :myleague
      get :edit
    end
  end

  resources :teams do
    collection do
      get :teams
      get :team_detail
    end
  end

  resources :playoffteams do
    collection do
      get :playoffteams
      get :playoffteams_chosen
    end
  end


  resources :profile do
    
  end

  resources :notifications do
    collection do
      get :invite_user
      get :accept_invitation
    end
  end

  root to: 'home#index'

end
